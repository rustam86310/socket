const express = require('express');
const socketio = require('socket.io');
const http = require('http');
const cors = require('cors');
const path = require('path');
var cookieParser = require('cookie-parser');
var session = require('express-session');
const { addUser, removeUser, getUser,
	getUsersInRoom,
	getAllUsers } = require("./users");
const { send_mail } = require('./sendMail');

const app = express();
const server = http.createServer(app);
const io = socketio(server);
const multer = require('multer');

const imageStorage = multer.diskStorage({
	// Destination to store image     
	destination: 'assets',
	filename: (req, file, cb) => {
		console.log("file====", file);
		cb(null, file.fieldname + '_' + Date.now()
			+ path.extname(file.originalname))
		// file.fieldname is name of the field (image)
		// path.extname get the uploaded file extension
	}
});

const imageUpload = multer({
	storage: imageStorage,
	limits: {
		fileSize: 1000000 // 1000000 Bytes = 1 MB
	},
	fileFilter(req, file, cb) {
		if (!file.originalname.match(/\.(png|jpg)$/)) {
			// upload only png and jpg format
			return cb(new Error('Please upload a Image'))
		}
		cb(undefined, true)
	}
})

app.use(cors())

app.use(cookieParser());

const oneMinute = 1000 * 6;

app.use(
	session({
		secret: "rdhsdfhfdgsfgds",
		resave: false,
		saveUninitialized: true,
		cookie: { maxAge: oneMinute },
	})
);

app.get('/', function (req, res) {
	if (req.session.page_views) {
		req.session.page_views++;
		res.send("You visited this page " + req.session.page_views + " times");
		//    req.session.destroy();
	} else {
		req.session.page_views = 1;
		res.send("Welcome to this page for the first time!");
	}
});

// For Single image upload
app.post('/uploadImage', imageUpload.single('selectedFile'), (req, res) => {
	console.log("req.file", req.file);
	res.status(200).send({message: req.file, result: true})
}, (error, req, res, next) => {
	res.status(400).send({ error: error.message })
})

app.get('/send_mail', async (req, res) => {
	send_mail(req, res);
})

io.on("connection", (socket) => {
	console.log("connection====", getAllUsers());
	socket.emit('message', {message:`welcome to room`});
	socket.on('join', ({ name, room }, callback) => {
		console.log("join=====", name, room);

		const { error, user } = addUser(
			{ id: socket.id, name, room });

		if (error) return callback(error);

		socket.emit('message', {
			user: getAllUsers(), text:
				`${user.name},
			welcome to room ${user.room}.`
		});

		socket.join(user.room);

		io.to(user.room).emit('roomData', {
			room: user.room,
			users: getUsersInRoom(user.room)
		});
		callback();
	})

	socket.on('sendMessage', (message, callback) => {
		console.log("sendMessage====");

		const user = getUser(socket.id);

		socket.broadcast.to(user.room)
			.emit('recieveMessage', { user: user.name, text: message }
			);
		// io.to(user.room).emit('message',
		// 	{ user: user.name, text: message });

		// io.to(user.room).emit('roomData', {
		// 	room: user.room,
		// 	users: getUsersInRoom(user.room)
		// });
		callback();
	})

	socket.on("disconnect", (reason) => {
		if (reason === "io server disconnect") {
			// the disconnection was initiated by the server, you need to reconnect manually
			socket.connect();
		}
		// else the socket will automatically try to reconnect
		console.log("disconnect====", socket.id);
		const user = removeUser(socket.id);
		console.log("user", user);
		if (user) {
			io.to(user.room).emit('message', { user: 'admin', text: `${user.name} had left` });
		}
	});

})

server.listen(process.env.PORT || 5000,
	() => console.log(`Server has started.5000`));
